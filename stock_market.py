import numpy as np
import typing


class StockMarket(object):

    def __init__(self, b0, stock_prices: np.array, interest_factor: float):
        """
        Initializes the relevant parameters

        :param b0: The budget available on day 0
        :param stock_prices: 2D numpy array, where stock_prices[i, j] is the
        price of stock j on day i
        :param interest_factor: The interest factor that is allocated to money
        that is on the bank (not invested in stocks)
        """
        self.b0 = b0
        self.stock_prices = stock_prices
        self.interest_factor = interest_factor
        # below the memory component of the dynamic programming solution
        # feel free to add or adjust
        self.n_portfolios = 2 ** self.stock_prices.shape[0]
        self.n_days = self.stock_prices.shape[1]
        self.memory = np.zeros(
            shape=(self.n_portfolios, self.n_days), dtype=float)

    @staticmethod
    def portfolio_has_stock(portfolio_idx: int, stock_idx: int) -> bool:
        """
        Determine for a given portfolio whether a given stock was in it.
        :param portfolio_idx: Index of the portfolio, expressed as int
        :param stock_idx: index of the stock
        :return: whether the stock was present in the portfolio
        """
        if not (portfolio_idx >> stock_idx) % 2:
            return False
        return True



    def calculate_transaction(self, day_idx: int, portfolio_idx_yesterday: int,
                              portfolio_idx_today: int) -> float:
        """
        Given a two stock portfolios (the one of today and the one of
        yesterday), determine the cost of the transaction. Note that each stock
        that was in yesterdays portfolio is sold and will count positive to the
        transaction, whereas each stock that was not in yesterdays portfolio and
        is in today's portfolio was bought, and will count negatively to the
        transaction.

        :param day_idx: the index of today's day (note that yesterday was
        day_idx - 1)
        :param portfolio_idx_yesterday: the index of today's portfolio,
        expressed as integer
        :param portfolio_idx_today: the index of yesterday's portfolio,
        expressed as integer

        :return: the transaction cost from yesterday's portfolio to today's
        portfolio
        """
        # print(day_idx, portfolio_idx_yesterday, portfolio_idx_today)
        # bstr_1 = format(portfolio_idx_today, '08b')
        # bstr_2 = format(portfolio_idx_yesterday, '08b')
        # bstr_3 = format(day_idx, '08b')
        # print(bstr_1, bstr_2, bstr_3)
        # print(self.stock_prices, self.interest_factor)
        # print(self.b0)
        if portfolio_idx_yesterday == 0 and portfolio_idx_today == 0:
            return 0
        else:
            total_cost = 0
            total_revenue = 0
            for i in range(portfolio_idx_yesterday):
                if self.portfolio_has_stock(portfolio_idx_yesterday, i):
                    total_revenue += self.stock_prices[i, day_idx]
            for j in range(portfolio_idx_today):
                if self.portfolio_has_stock(portfolio_idx_today, j):
                    total_cost += self.stock_prices[j, day_idx]
            return total_revenue - total_cost


    def dynamic_programming_bottom_up(self) -> None:
        """
        Fills the complete memory table in a bottom up fashion.
        """

        print(self.b0, self.n_days, self.stock_prices, self.n_portfolios, self.memory, self.interest_factor)

        self.memory[0, 0] = self.b0
        for i in range(1, len(self.memory)):
            total_cost = 0
            for j in range(8):
                if self.portfolio_has_stock(i, j):
                    total_cost += self.stock_prices[j, 0]
            self.memory[i, 0] = self.b0 - total_cost

        for day in range(1, self.n_days):
            for port in range(self.n_portfolios):
                max_num = -99999999

                for yestd in range(self.n_portfolios):
                    if self.memory[yestd, day - 1] >= 0:
                        max_profit = self.memory[yestd, day - 1] * self.interest_factor \
                            + self.calculate_transaction(day, yestd, port)

                        if max_profit > max_num:
                            max_num = max_profit
                self.memory[port, day] = max_num




        print()
        print()
        print(self.memory)

    def max_gain_on_day(self, day: int) -> float:
        """
        Returns for a given day the maximum budget that can be obtained
        :param day: The day we are interested in
        :return: The maximum budget that can be obtained
        """
        max_gain = 0
        for i in range(self.n_portfolios):
            if self.memory[i][day] > max_gain:
                max_gain = self.memory[i][day]
        return max_gain

    def backtracing_portfolio(self) -> typing.List[int]:
        """Returns a sequence of portfolios how to come to the optimal solution

        Optional - you can still pass the assignment if you do not hand this in,
        however it will count towards your grade.

        :return: For each day, the portfolio index to obtain the optimal
        solution
        """
        optimal = [0]

        for day in range(1, self.n_days):
            max = -99
            max_p = 0
            for port in range(self.n_portfolios):
                if (self.memory[port, day] - self.memory[port, day - 1]) > max:
                    max = self.memory[port, day] - self.memory[port, day - 1]
                    max_p = port
            optimal.append(max_p)

        return optimal

